#include "bouquettest.h"

#include <libipv6y/bouquet.h>

#include <QtNetwork/QHostAddress>

#include <QtTest>

typedef QList<QHostAddress> HostAddressList;

Q_DECLARE_METATYPE( HostAddressList )
Q_DECLARE_METATYPE( QHostAddress );

void BouquetTest::testBasicActions() const
{
    const QHostAddress address( QStringLiteral( "dead::beaf" ) );

    Bouquet bouquet;

    QCOMPARE( bouquet.isEmpty(), true );
    QCOMPARE( bouquet.size(), 0 );
    QCOMPARE( bouquet.contains( address ), false );

    QCOMPARE( bouquet.addAddress( address ), true );

    QCOMPARE( bouquet.isEmpty(), false );
    QCOMPARE( bouquet.size(), 1 );
    QCOMPARE( bouquet.contains( address ), true );

    QCOMPARE( bouquet.takeAddress(), address );

    QCOMPARE( bouquet.isEmpty(), true );
    QCOMPARE( bouquet.size(), 0 );
    QCOMPARE( bouquet.contains( address ), false );
}

void BouquetTest::testAddAddress() const
{
    QFETCH( HostAddressList, addBefore );
    QFETCH( QHostAddress, address );
    QFETCH( bool, expected );

    Bouquet bouquet;

    foreach ( const QHostAddress &addr, addBefore ) {
        bouquet.addAddress( addr );
    }

    QCOMPARE( bouquet.addAddress( address ), expected );
}

void BouquetTest::testAddAddress_data() const
{
    QTest::addColumn<HostAddressList>( "addBefore" );
    QTest::addColumn<QHostAddress>( "address" );
    QTest::addColumn<bool>( "expected" );

    // w/o adding anything beforehand
    {
        typedef QPair<QHostAddress, bool> Test;
        QList<Test> tests;

        tests << Test( QHostAddress(), false )
              << Test( QHostAddress( QHostAddress::LocalHost ), false )
              << Test( QHostAddress( QHostAddress::LocalHostIPv6 ), false )
              << Test( QHostAddress( QStringLiteral( "dead::beaf" ) ), true );

        foreach ( const Test &test, tests ) {
            QTest::newRow( qPrintable( test.first.toString() ) )
                << HostAddressList() << test.first << test.second;
        }
    }

    QTest::newRow( "Duplicate" )
        << ( HostAddressList()
             << QHostAddress( QStringLiteral( "dead::beaf" ) ) )
        << QHostAddress( QStringLiteral( "dead::beaf" ) ) << false;
}

QTEST_GUILESS_MAIN( BouquetTest )
