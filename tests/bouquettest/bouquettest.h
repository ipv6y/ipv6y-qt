#ifndef BOUQUETTEST_H
#define BOUQUETTEST_H

#include <QtCore/QObject>

class BouquetTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testBasicActions() const;
    void testAddAddress() const;
    void testAddAddress_data() const;
};

#endif // BOUQUETTEST_H
