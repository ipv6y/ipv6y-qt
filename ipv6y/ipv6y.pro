DEPTH = ..
include( $${DEPTH}/global.pri )

TARGET = ipv6y

include( $${TARGET_BIN} )

CONFIG -= app_bundle
CONFIG += console

SOURCES += main.cpp

QT += network
QT -= gui

LINK_TARGET = libipv6y
LINK_SRC_DIR = $${IPV6YLIB_SRC_DIR}
include( $${LINK_LIBRARY} )

LINK_TARGET = libsocks
LINK_SRC_DIR = $${SOCKSLIB_SRC_DIR}
include( $${LINK_LIBRARY} )
