#include <libipv6y/bouquet.h>
#include <libipv6y/ip6tablescommandfactory.h>
#include <libipv6y/ipv6yfactory.h>

#include <SocksLib/SocksServer.h>

#include <QtCore/QCommandLineParser>
#include <QtCore/QCoreApplication>
#include <QtNetwork/QNetworkInterface>

enum OS { UnsupportedOS, Linux, OSX };

static OS thisOS()
{
#ifdef Q_OS_LINUX
    return Linux;
#elif defined( Q_OS_OSX )
    return OSX;
#else
#error Unsupported OS
    return UnsupportedOS;
#endif
}

static QString defaultInterface( const OS os = thisOS() )
{
    switch ( os ) {
        case Linux:
            return QStringLiteral( "eth0" );
        case OSX:
            return QStringLiteral( "en0" );
        case UnsupportedOS:
            Q_ASSERT( false );
    }
}

static bool initBouquet( Bouquet *bouquet,
                         const QString &interfaceName,
                         QString *errMsg )
{
    Q_ASSERT( bouquet );
    Q_ASSERT( errMsg );

    const QNetworkInterface &interface =
        QNetworkInterface::interfaceFromName( interfaceName );

    if ( !interface.isValid() ) {
        *errMsg =
            QStringLiteral( "Found no interface '%1'" ).arg( interfaceName );
        return false;
    }
    if ( ( interface.flags() & QNetworkInterface::IsUp ) !=
         QNetworkInterface::IsUp ) {
        *errMsg = QStringLiteral( "Interface '%1' is inactive." )
                      .arg( interfaceName );
        return false;
    }
    const QList<QNetworkAddressEntry> &addresses = interface.addressEntries();
    foreach ( const QNetworkAddressEntry &address, addresses ) {
        // FIXME: Avoid deprecated addresses. Looks like we cannot do that with
        // Qt, see QTBUG-48589
        if ( bouquet->addAddress( address.ip() ) ) {
            qDebug( "Added address: %s",
                    qPrintable( address.ip().toString() ) );
        }
    }

    return true;
}

int main( int argc, char *argv[] )
{
    QCoreApplication app( argc, argv );

    app.setApplicationName( QStringLiteral( "ipv6y" ) );
    app.setApplicationVersion( QStringLiteral( "1.0.0" ) );
    app.setOrganizationName( QStringLiteral( "Universit\u00E4t Hamburg" ) );
    app.setOrganizationDomain(
        QStringLiteral( "de.uni-hamburg.informatik.svs" ) );

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption sshOpt(
        QStringLiteral( "ssh" ),
        QCoreApplication::translate(
            "main",
            "Execute routing commands on remote machine. If not set the local "
            "machine is used. The used machine needs to be Linux with Kernel "
            "3.7 or later and configured for IPv6 SNAT. See README for "
            "details. Password authentication is not supported, use public key "
            "authentication instead." ),
        QCoreApplication::translate( "main", "user@host" ) );
    parser.addOption( sshOpt );

    QCommandLineOption clientInterfaceOpt(
        QStringLiteral( "client-interface" ),
        QCoreApplication::translate(
            "main", "The interface of the client. (Default: %1)" )
            .arg( defaultInterface() ),
        QCoreApplication::translate( "main", "interface" ) );
    clientInterfaceOpt.setDefaultValue( defaultInterface() );
    parser.addOption( clientInterfaceOpt );

    QCommandLineOption routerInterfaceOpt(
        QStringLiteral( "router-interface" ),
        QCoreApplication::translate( "main",
                                     "The network interface where the "
                                     "masquerading is applied. If SSH is used, "
                                     "this will be the outgoing interface of "
                                     "the remote host. (Default: %1)" )
            .arg( defaultInterface( Linux ) ),
        QCoreApplication::translate( "main", "interface" ) );
    routerInterfaceOpt.setDefaultValue( defaultInterface( Linux ) );
    parser.addOption( routerInterfaceOpt );

    parser.process( app );

    const bool withSsh = parser.isSet( sshOpt );
    QString sshUser;
    QString sshHost;
    if ( withSsh ) {
        const QStringList sshArgument =
            parser.value( sshOpt )
                .split( QStringLiteral( "@" ), QString::SkipEmptyParts );

        if ( sshArgument.count() != 2 ) {
            qFatal( "Invalid ssh argument, please use the format: user@host" );
            return 1;
        }

        sshUser = sshArgument[ 0 ];
        sshHost = sshArgument[ 1 ];
    }

    const QString table = QStringLiteral( "nat" );
    const QString clientInterface = parser.value( clientInterfaceOpt );
    const QString routerInterface = parser.value( routerInterfaceOpt );

    Bouquet bouquet;
    // TODO: add static address(es)
    QString errMsg;
    if ( !initBouquet( &bouquet, clientInterface, &errMsg ) ) {
        qFatal( "%s", qPrintable( errMsg ) );
        return 1;
    }

    if ( bouquet.isEmpty() ) {
        qFatal( "Failed to initialize bouquet." );
        return 1;
    }

    Ip6tablesCommandFactory commandFactory( table, sshUser, sshHost );

    Command flushCmd = commandFactory.getFlushCommand();
    if ( !flushCmd.execute() ) {
        qFatal( "Failed to flush table '%s': %s",
                qPrintable( table ),
                qPrintable( flushCmd.errorString() ) );
        return 1;
    } else {
        qDebug( "Flushed table '%s'", qPrintable( table ) );
    }

    Command masquerdeCmd =
        commandFactory.getMasqueradeCommand( routerInterface );
    if ( !masquerdeCmd.execute() ) {
        qFatal(
            "Failed to enable masquerading for interface '%s' in table '%s'",
            qPrintable( routerInterface ),
            qPrintable( table ) );
        return 1;
    } else {
        qDebug( "Enabled masquerading for interface '%s' in table '%s'",
                qPrintable( routerInterface ),
                qPrintable( table ) );
    }

    IPv6yFactory factory( &commandFactory, &bouquet );

    SocksServer server;
    server.setConnectionFilterFactory( &factory );

    server.start();

    if ( !server.isStarted() ) {
        qWarning() << QObject::tr(
            "Failed to start server, maybe the port is in use?. Shutting "
            "down." );
        return 1;
    }

    return app.exec();
}
