#include "bouquet.h"

static bool addressIsValid( const QHostAddress &address )
{
    return !address.isNull() && !address.isLoopback() &&
           ( address.protocol() == QAbstractSocket::IPv6Protocol ) &&
           address.scopeId().isEmpty();
}

Bouquet::Bouquet()
{
}

Bouquet::~Bouquet()
{
}

bool Bouquet::addAddress( const QHostAddress &address )
{
    if ( !addressIsValid( address ) || contains( address ) ||
         isStatic( address ) )
        return false;
    m_addresses.insert( address );
    Q_ASSERT( contains( address ) );
    return true;
}

bool Bouquet::addStaticAddress( const QHostAddress &address )
{
    if ( !addressIsValid( address ) || isStatic( address ) )
        return false;
    if ( contains( address ) )
        m_addresses.remove( address );
    Q_ASSERT( !contains( address ) );
    m_staticAddresses.insert( address );
    Q_ASSERT( m_staticAddresses.contains( address ) );
    return true;
}

bool Bouquet::contains( const QHostAddress &address ) const
{
    return m_addresses.contains( address );
}

bool Bouquet::isEmpty() const
{
    return m_addresses.isEmpty();
}

bool Bouquet::isStatic( const QHostAddress &address ) const
{
    return m_staticAddresses.contains( address );
}

int Bouquet::size() const
{
    return m_addresses.size();
}

QHostAddress Bouquet::takeAddress()
{
    if ( isEmpty() )
        return QHostAddress();
    const QHostAddress address =
        m_addresses.values().takeAt( qrand() % size() );
    m_addresses.remove( address );
    Q_ASSERT( !contains( address ) );
    return address;
}
