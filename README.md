# ipv6y

## Prerequesites

* [Qt 5](http://www.qt.io/download-open-source/)
* Linux Kernel version 3.7 or higher, configured with IPv6 NAT Support (see below)

### Linux Kernel Configuration

A kernel version 3.7+ is required according to
[`man ip6tables`](http://ipset.netfilter.org/ip6tables.man.html)

To enable IPv6 NAT support you need to build your kernel with:

```
[*] Networking Support  --->
    Networking options  --->
        [*] Network packet filtering framework (Netfilter)  --->
            Core Netfilter Configuration  --->
                <M> IPv4/IPv6 redirect support
                […]
                -M- "SNAT and DNAT" targets support
            IPv6: Netfilter Configuration  --->
                -M- IPv6 NAT
                -M-   IPv6 masquerade support
                […]
                <M> ip6tables NAT support
                <M>   MASQUERADE target support
                <M>   NPT (Network Prefix transllation) target support
```

You can test if IPv6 NAT support is enabled in your current kernel by executing:

```sh
ip6tables --table nat --list
```

If IPv6 NAT support is not enabled by your kernel `ip6tables` will exit with a
non-zero exit code (3) and will display the following error message:

```
modprobe: ERROR: could not insert 'ip6_tables': Invalid argument
ip6tables v1.4.21: can't initialize ip6tables table `nat': Table does not exist (do you need to insmod?)
Perhaps ip6tables or your kernel needs to be upgraded.
```

## Basic Idea

To view the content of the `nat` table:

```sh
ip6tables --table nat --list POSTROUTING --verbose
```

To start anew you might want to flush the `nat` table by issuing:

```sh
ip6tables --table nat --flush
```

First we need to enable masquerading for the outgoing interface, ie. `eth0`,
which allows us to amend the IP addresses in the filtered packets:

```sh
ip6tables --table nat --append POSTROUTING --out-interface eth0 -j MASQUERADE
```

Then we replace the source address of outgoing packets and replace it with one
address of our bucket, fe. `2001:db8::1/34` if the destination address is fe.
`2001:db8::2/34` using the TCP protocol:

```sh
ip6tables --table nat --append POSTROUTING --protocol tcp --destination 2001:db8::2 -j SNAT --to-source 2001:db8::1
```

To remove the previous rule again:

```sh
ip6tables --table nat --delete POSTROUTING --protocol tcp --destination 2001:db8::2 -j SNAT --to-source 2001:db8::1
```

## Simple Test Setup

Just start a web server with PHP support and add the following PHP script, which
shows the IP address used by the client:

```php
<?php
header( 'Content-Type: text/plain' );
print( $_SERVER[ 'REMOTE_ADDR' ] );
?>
```

## Debugging `ip6tables`
See [iptables debugging](http://backreference.org/2010/06/11/iptables-debugging/)

## Using Remote (SSH)

`ipv6y` can connect to a different host (fe. Linux router) which is then used to apply filtering rules.

Password authentication is not supported. Use public key authentication instead.

To connect to the host `host` as user `user`:

```sh
ipv6y --ssh user@host
```

Since an SSH connection is established for each request the performance can be
vastly improved by setting up a control master connection beforehand.

Add the following to `~/.ssh/config`:
```
ControlMaster auto
ControlPath ~/.ssh/control:%r@%h:%p
ControlPersist yes
```

You might need to disconnect manually afterwards:
```sh
ssh -O exit user@host
```
