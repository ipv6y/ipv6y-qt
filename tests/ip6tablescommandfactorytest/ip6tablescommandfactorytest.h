#ifndef IP6TABLESCOMMANDFACTORYTEST_H
#define IP6TABLESCOMMANDFACTORYTEST_H

#include <QtCore/QObject>

class Ip6tablesCommandFactoryTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testFlushCommand() const;
    void testRemoteFlushCommand() const;
};

#endif // IP6TABLESCOMMANDFACTORYTEST_H
