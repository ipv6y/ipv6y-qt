isEmpty( DEPTH ): error( "$${_PRO_FILE_}: DEPTH not set!" )

# Define directory paths
ROOT_SRC_DIR = $${PWD}
ROOT_OUT_DIR = $$clean_path( $${OUT_PWD}/$${DEPTH} )

3RDPARTY_SRC_DIR = $${ROOT_SRC_DIR}/3rdparty

SOCKS_SRC_DIR = $${3RDPARTY_SRC_DIR}/Qt-Socks-Server
SOCKSLIB_SRC_DIR = $${SOCKS_SRC_DIR}/SocksLib

IPV6YLIB_SRC_DIR = $${ROOT_SRC_DIR}/libipv6y

BIN_OUT_DIR = $${ROOT_OUT_DIR}/bin
LIB_OUT_DIR = $${ROOT_OUT_DIR}/lib
TEST_OUT_DIR = $${BIN_OUT_DIR}/tests

win32 {
    CONFIG( release, debug|release ) {
        LIB_OUT_DIR = $${LIB_OUT_DIR}/release
    } else:CONFIG( debug, debug|release ) {
        LIB_OUT_DIR = $${LIB_OUT_DIR}/debug
    }
}

# Define target scripts
TARGET_BIN = $${ROOT_SRC_DIR}/bin.pri # for applications
TARGET_LIB = $${ROOT_SRC_DIR}/lib.pri # for libraries
TARGET_TEST = $${ROOT_SRC_DIR}/test.pri # for tests
LINK_LIBRARY = $${ROOT_SRC_DIR}/link.pri # for linking libraries

# Global configuration
CONFIG -= ordered
CONFIG *= c++11

osx {
    # Allow directly running applications linking against this library.
    # The path used to link the library will be absolute as can be seen when
    # invoking 'otool -L /path/to/binary' and therefore this does NOT replace
    # the deployment steps necessary before moving or sharing the application.
    # See: http://doc.qt.io/qt-5/osx-deployment.html
    QMAKE_LFLAGS_SONAME = -Wl,-install_name,$${LIB_OUT_DIR}/
}

QMAKE_CXXFLAGS *= $${QMAKE_CXXFLAGS_HIDESYMS}

gcc:!clang {
    osx {
        QMAKE_LFLAGS_SHLIB *= -undefined=error
    } else {
        QMAKE_LFLAGS_SHLIB *= -Wl,--no-undefined
    }
}

gcc {
    QMAKE_CXXFLAGS *= -pedantic
    QMAKE_CXXFLAGS *= -Wall
    QMAKE_CXXFLAGS *= -Wextra
}

DEFINES += QT_NO_CAST_FROM_ASCII
DEFINES += QT_NO_CAST_TO_ASCII
DEFINES += QT_NO_CAST_FROM_BYTEARRAY
