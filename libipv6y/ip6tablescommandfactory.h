#ifndef IP6TABLESCOMMANDFACTORY_H
#define IP6TABLESCOMMANDFACTORY_H

#include "libipv6y.h"

#include "remotecommandfactory.h"

#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QHostAddress>

class IPV6YSHARED_EXPORT Ip6tablesCommandFactory : private RemoteCommandFactory
{
public:
    explicit Ip6tablesCommandFactory( const QString &table );

    explicit Ip6tablesCommandFactory( const QString &table,
                                      const QString &user,
                                      const QString &host );

    ~Ip6tablesCommandFactory();

    QString table() const;

    Command getFlushCommand() const;

    Command getListCommand() const;

    Command getMasqueradeCommand( const QString &outInterface ) const;

    Command getSNATCommand( const bool append,
                            const QAbstractSocket::SocketType protocol,
                            const QHostAddress &destination,
                            const QHostAddress &toSource,
                            const QHostAddress &source = QHostAddress() ) const;

private:
    Command getTableCommand() const;

    QString m_table;
};

#endif // IP6TABLESCOMMANDFACTORY_H
