#ifndef BOUQUET_H
#define BOUQUET_H

#include "libipv6y.h"

#include <QtCore/QSet>
#include <QtNetwork/QHostAddress>

class IPV6YSHARED_EXPORT Bouquet
{
public:
    explicit Bouquet();

    virtual ~Bouquet();

    bool addAddress( const QHostAddress &address );

    bool addStaticAddress( const QHostAddress &address );

    bool contains( const QHostAddress &address ) const;

    bool isEmpty() const;

    bool isStatic( const QHostAddress &address ) const;

    int size() const;

    QHostAddress takeAddress();

private:
    QSet<QHostAddress> m_addresses;
    QSet<QHostAddress> m_staticAddresses;
};

#endif // BOUQUET_H
