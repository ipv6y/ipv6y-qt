#ifndef IPV6YFACTORY_H
#define IPV6YFACTORY_H

#include "libipv6y.h"

#include <SocksLib/filters/ConnectionFilterFactory.h>

class Bouquet;
class Ip6tablesCommandFactory;

class IPV6YSHARED_EXPORT IPv6yFactory : public ConnectionFilterFactory
{
    Q_OBJECT

public:
    explicit IPv6yFactory( const Ip6tablesCommandFactory *commandFactory,
                           Bouquet *bouquet,
                           QObject *parent = nullptr );
    virtual ~IPv6yFactory();

    virtual ConnectionFilter *newConnectionFilter(
        const QHostAddress &address, const quint16 port ) const Q_DECL_OVERRIDE;

protected:
    const Ip6tablesCommandFactory *m_commandFactory;
    Bouquet *m_bouquet;
};

#endif // IPV6YFACTORY_H
