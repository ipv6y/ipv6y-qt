#ifndef REMOTECOMMANDFACTORY_H
#define REMOTECOMMANDFACTORY_H

#include "command.h"

#include <QtCore/QString>

class RemoteCommandFactory
{
public:
    explicit RemoteCommandFactory();

    explicit RemoteCommandFactory( const QString &user, const QString &host );

    virtual ~RemoteCommandFactory();

    virtual Command getCommand( const QString &program ) const;

private:
    QString m_user;
    QString m_host;
};

#endif // REMOTECOMMANDFACTORY_H
