#ifndef IPV6Y_H
#define IPV6Y_H

#include "libipv6y.h"

#include <SocksLib/filters/ConnectionFilter.h>

class Command;
class Bouquet;
class Ip6tablesCommandFactory;

class IPV6YSHARED_EXPORT IPv6y : public ConnectionFilter
{
    Q_OBJECT

public:
    explicit IPv6y( const Ip6tablesCommandFactory *commandFactory,
                    Bouquet *bouquet,
                    const QHostAddress &clientAddress,
                    const quint16 clientPort,
                    QObject *parent = nullptr );

    virtual ~IPv6y();

    virtual bool isConnectionToAddressAllowed(
        const QHostAddress &address, const quint16 port ) Q_DECL_OVERRIDE;

    virtual bool isConnectionToDomainAllowed(
        const QString &domain, const quint16 port ) Q_DECL_OVERRIDE;

protected:
    const Ip6tablesCommandFactory *m_commandFactory;
    Bouquet *m_bouquet;
    Command *m_undoCommand;
};

#endif // IPV6Y_H
