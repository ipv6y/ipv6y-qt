DEPTH = ../..
include( $${DEPTH}/global.pri )

TARGET = ip6tablescommandfactorytest

include( $${TARGET_TEST} )

TEMPLATE = app

CONFIG += console
CONFIG -= app_bundle

HEADERS += ip6tablescommandfactorytest.h
HEADERS += $${IPV6YLIB_SRC_DIR}/command.h
HEADERS += $${IPV6YLIB_SRC_DIR}/ip6tablescommandfactory.h
HEADERS += $${IPV6YLIB_SRC_DIR}/remotecommandfactory.h

SOURCES += ip6tablescommandfactorytest.cpp
SOURCES += $${IPV6YLIB_SRC_DIR}/command.cpp
SOURCES += $${IPV6YLIB_SRC_DIR}/ip6tablescommandfactory.cpp
SOURCES += $${IPV6YLIB_SRC_DIR}/remotecommandfactory.cpp

INCLUDEPATH += $$clean_path( $${IPV6YLIB_SRC_DIR}/.. )

QT += network
QT += testlib
QT -= gui
