isEmpty( LIB_OUT_DIR ): error( "$${_PRO_FILE_}: LIB_OUT_DIR not set!" )
isEmpty( BIN_OUT_DIR ): error( "$${_PRO_FILE_}: BIN_OUT_DIR not set!" )

TEMPLATE = lib

DESTDIR = $${LIB_OUT_DIR}
DLLDESTDIR = $${LIB_OUT_DIR}

unix {
    target.path = /usr/local/lib
    INSTALLS += target
}
