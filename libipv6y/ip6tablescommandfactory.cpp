#include "ip6tablescommandfactory.h"

Ip6tablesCommandFactory::Ip6tablesCommandFactory( const QString &table )
    : RemoteCommandFactory(), m_table( table )
{
}

Ip6tablesCommandFactory::Ip6tablesCommandFactory( const QString &table,
                                                  const QString &user,
                                                  const QString &host )
    : RemoteCommandFactory( user, host ), m_table( table )
{
}

Ip6tablesCommandFactory::~Ip6tablesCommandFactory()
{
}

QString Ip6tablesCommandFactory::table() const
{
    return m_table;
}

Command Ip6tablesCommandFactory::getTableCommand() const
{
    Command cmd =
        RemoteCommandFactory::getCommand( QStringLiteral( "ip6tables" ) );
    if ( !m_table.isNull() ) {
        cmd.addArgument( QStringLiteral( "--table" ) );
        cmd.addArgument( m_table );
    }
    return cmd;
}

Command Ip6tablesCommandFactory::getFlushCommand() const
{
    Command cmd = getTableCommand();
    cmd.addArgument( QStringLiteral( "--flush" ) );
    return cmd;
}

Command Ip6tablesCommandFactory::getListCommand() const
{
    Command cmd = getTableCommand();
    cmd.addArgument( QStringLiteral( "--list" ) );
    return cmd;
}

Command Ip6tablesCommandFactory::getMasqueradeCommand(
    const QString &outInterface ) const
{
    Command cmd = getTableCommand();
    cmd.addArgument( QStringLiteral( "--append" ) );
    cmd.addArgument( QStringLiteral( "POSTROUTING" ) );
    cmd.addArgument( QStringLiteral( "--out-interface" ) );
    cmd.addArgument( outInterface );
    cmd.addArgument( QStringLiteral( "--jump" ) );
    cmd.addArgument( QStringLiteral( "MASQUERADE" ) );
    return cmd;
}

Command Ip6tablesCommandFactory::getSNATCommand(
    const bool append,
    const QAbstractSocket::SocketType protocol,
    const QHostAddress &destination,
    const QHostAddress &toSource,
    const QHostAddress &source ) const
{
    Command cmd = getTableCommand();
    cmd.addArgument( append ? QStringLiteral( "--append" )
                            : QStringLiteral( "--delete" ) );
    cmd.addArgument( QStringLiteral( "POSTROUTING" ) );
    cmd.addArgument( QStringLiteral( "--protocol" ) );
    switch ( protocol ) {
        case QAbstractSocket::TcpSocket:
            cmd.addArgument( QStringLiteral( "tcp" ) );
            break;
        case QAbstractSocket::UdpSocket:
            cmd.addArgument( QStringLiteral( "udp" ) );
            break;
        case QAbstractSocket::UnknownSocketType:
            Q_ASSERT( false );
    }
    if ( !source.isNull() ) {
        cmd.addArgument( QStringLiteral( "--source" ) );
        cmd.addArgument( source.toString() );
    }
    cmd.addArgument( QStringLiteral( "--destination" ) );
    cmd.addArgument( destination.toString() );
    cmd.addArgument( QStringLiteral( "--jump" ) );
    cmd.addArgument( QStringLiteral( "SNAT" ) );
    cmd.addArgument( QStringLiteral( "--to-source" ) );
    cmd.addArgument( QStringLiteral( "[%1]" ).arg( toSource.toString() ) );
    return cmd;
}
