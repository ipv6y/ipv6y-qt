#include "command.h"

#include <QtCore/QProcess>

#include <QtDebug>

Command::Command( const QString &program, const QStringList &arguments )
    : m_program( program ), m_arguments( arguments )
{
}

Command::~Command()
{
}

void Command::addArgument( const QString &argument )
{
    m_arguments << argument;
}

QStringList Command::arguments() const
{
    if ( isSsh() ) {
        return QStringList()
               << QStringLiteral( "%1@%2" ).arg( m_sshUser ).arg( m_sshHost )
               << ( QStringList() << m_program << m_arguments )
                      .join( QStringLiteral( " " ) );
    }
    return m_arguments;
}

QString Command::errorString() const
{
    return m_errorString;
}

bool Command::execute()
{
    qDebug() << "Executing:" << program() << arguments();

    QProcess process;
    process.start( program(), arguments() );
    if ( !process.waitForFinished( 10000 ) ) {
        m_errorString = process.errorString();
        if ( m_errorString.isNull() )
            m_errorString = QStringLiteral( "" );
    }

    m_stdOut = process.readAllStandardOutput();
    m_stdErr = process.readAllStandardError();

    if ( ( process.exitStatus() != QProcess::NormalExit ) ||
         ( process.exitCode() != 0 ) ) {
        m_errorString = process.errorString();
        if ( m_errorString.isEmpty() ) {
            m_errorString = QString::fromUtf8( m_stdErr );
        }
        if ( m_errorString.isNull() ) {
            m_errorString = QStringLiteral( "" );
        }
    }

    if ( !m_stdOut.isEmpty() ) {
        qDebug() << "stdout:" << QString::fromUtf8( m_stdOut );
    }
    if ( !m_stdErr.isEmpty() ) {
        qDebug() << "stderr:" << QString::fromUtf8( m_stdErr );
    }

    return m_errorString.isNull();
}

QString Command::program() const
{
    if ( isSsh() )
        return QStringLiteral( "ssh" );
    return m_program;
}

bool Command::isSsh() const
{
    return !m_sshHost.isNull();
}

void Command::setSsh( const QString &user, const QString &host )
{
    m_sshUser = user;
    m_sshHost = host;
}
